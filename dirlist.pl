#!/usr/bin/perl

#Simple script that spits on the names of movies from a specific folder. How it's done is simple in my case I noticed the '-'
#was the main thing that seperated the title of the movie and extraneous info so I used a split function to split the name
#and the extraneous info array element 0 and 1 respectively and just outputted 0. If there is no '-' then it will just print
#out everything. It also writes the same thing to a text file.
use feature 'say';


#Change directory to where you have the movies located.
opendir DIR, "H:\Movies";

@files = readdir(DIR);
open TEXT, '>> Movies.txt' or die "Failed opening File";

foreach $lines (@files){

	$lines =~ s/\./ /g;
	
	if(($regMatch) = $lines =~ /(\d{4})/){
		
		@film = split(/$regMatch/, $lines);
		
		say $film[0];
		say TEXT $film[0];
		}
		else{
			#say $lines;
			say TEXT $lines;
		}
	

}
closedir DIR;
