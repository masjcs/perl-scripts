#!/usr/bin/perl
use warnings;
use strict;

#USAGE: perl matrix_generator.pl textname
#Description: Creates a random 26x26 text file of random characters.
#This was primarily created to make matrices for a vigenere cipher

my $randomNumber;		#The Generated value
my $columnCeiling = 26;		#Number of characters per line
my $rowCeiling = 26;		#Number of columns for matrix
my $range = 93;			#The last number of a valid character in ASCII
my $minimum = 33;		#The first number of a valid character in ASCII
my $saveLocation = "Z:\\";	#The location of where the file is being written to
my $fileName = $ARGV[0];	#The name of the file
my $absolutePath = $saveLocation.$fileName.".txt";	#Z:\\some.txt
	
	open TEXT, ">> $absolutePath" or die "Failed opening File";
	for(my $i = 0; $i < $rowCeiling; $i++){
		for(my $i = 0; $i < $columnCeiling; $i++){
			$randomNumber = int(rand($range)) + $minimum;		#Generates number between 33-126
			print TEXT chr($randomNumber);		#Converts Decimal to its ASCII value
		}
		print TEXT "\n";
	}
	close TEXT;